/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxgameui;

import java.io.Serializable;
import jdk.nashorn.api.tree.BreakTree;

/**
 *
 * @author ASUS
 */
public class Table implements Serializable{

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;

    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");

        }
    }
    public char getRowCol(int row, int col) {
        return table[row][col];
    }

    public boolean setRowCol(int row, int col) {
        if(isFinish()){
            return false;
        }
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            checkWin();
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkCrossX1() {
        if (table[0][0] == 'x') {
            if (table[1][1] == 'x') {
                if (table[2][2] == 'x') {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                }
            }
        }
    }

    void checkCrossX2() {
        if (table[0][2] == 'x') {
            if (table[1][1] == 'x') {
                if (table[2][0] == 'x') {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                }
            }
        }
    }

    void checkCrossO1() {
        if (table[0][0] == 'o') {
            if (table[1][1] == 'o') {
                if (table[2][2] == 'o') {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                }
            }
        }
    }

    void checkCrossO2() {
        if (table[0][2] == 'o') {
            if (table[1][1] == 'o') {
                if (table[2][0] == 'o') {
                    finish = true;
                    winner = currentPlayer;
                    setStatWinLose();
                }
            }
        }
    }

    void checkDraw() {
        for (int col = 0; col < 3; col++) {
            for (int row = 0; row < 3; row++) {
                if (table[row][col] == '-') {

                    return;
                }
            }
        }
        finish = true;
        playerO.draw();
        playerX.draw();

    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkCrossX1();
        checkCrossX2();
        checkCrossO1();
        checkCrossO2();
        checkDraw();

    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }

}
